﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour
{

    int max, min, avg, count;
    
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            count++;
            min = avg;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            count++;
            max = avg;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Your guess is : " + avg);
            Debug.Log("You tried " + count + " time.");
            Start();
        }
    }


    // function 
    void StartGame() 
    {
        min = 1;
        max = 100;
        avg = 50;
        count = 0;

        Debug.Log("Welcome to number wizard");
        Debug.Log("Pick a number, don't tell me what it is ...");
        Debug.Log("Highest number is: " + max);
        Debug.Log("Lowest number is: " + min);
        Debug.Log("Tell me if your number is higher or lower than " + avg);
        Debug.Log("Push up = Higher, Push down = Lower, Push Enter = Correct");
        max = max + 1;
    }

    void NextGuess()
    {
        avg = (max + min) / 2;
        Debug.Log("Higher or Lower than this : " + avg);
    }


}
