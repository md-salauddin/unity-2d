﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// It will create a State menu into menu(right click on unity)
// Sort of Model class
[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject
{
    /**
     * 14 -> Create a textarea on State(menu) 
     *   that is 14px in width (minimum)
     * 10 -> Scrolling if huge text shown after 10
     * */
    [TextArea(14, 10)] [SerializeField] string storyText;
    [SerializeReference] State[] nextStates; 


    // Method
    public string GetStateText() {
        return storyText;
    }

    public State[] GetNextState() {
        return nextStates;
    }

}
