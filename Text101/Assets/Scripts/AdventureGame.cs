﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureGame : MonoBehaviour
{
    // Serialize means if we add it, it will show
    // into unity object field
    [SerializeReference] Text textComponent;
    [SerializeReference] State startingState;

    // ctrl + R + R = for rename any variable name
    
    State state;

    // Start is called before the first frame update
    void Start()
    {
        state = startingState;
        // In TextComponent(Unity) lots component exist
        // .text means just pick text from other all component.
        textComponent.text = state.GetStateText();
    }

    // Update is called once per frame
    void Update()
    {
        ManageState();
    }

    private void ManageState()
    {
        // if we use var, it will auto detect variable type 
        var nextStates = state.GetNextState();

        for (int i=0; i<nextStates.Length; i++) 
        {
            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                state = nextStates[i];
            }
        }

        textComponent.text = state.GetStateText();

    }


}
