﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreLogic : MonoBehaviour
{

    [SerializeField] int max, min;
    [SerializeField] Text guessText;
    int avg;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // On Button Click
    public void OnPressHigher()
    {
        min = avg + 1;
        NextGuess();
    }

    public void OnPressLower()
    {
        max = avg - 1;
        NextGuess();
    }

    // function 
    void StartGame()
    {
        NextGuess();
        max = max + 1;
    }

    void NextGuess()
    {
        //avg = (max + min) / 2;
        avg = Random.Range(min, max + 1);
        guessText.text = avg.ToString();
    }


}
