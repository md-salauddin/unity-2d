﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    public void LoadNextScene() 
    {
        // Get the current scene index
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        // Load next scene when button clicked
        SceneManager.LoadScene(currentSceneIndex + 1); 
    }

    public void LoadStartScene()
    {
        // Load 0 no scene 
        SceneManager.LoadScene(0);
    }

    public void QuitTheGame()
    {
        // Exit the game
        Application.Quit();
    }


}
